#
# Copyright (c) 2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Abstract cookbook_name
cookbook_name = 'dynamic-attributes'

# Whitelisting: define which attributes may be rewrited by this cookbook

# Default is everything for replace
# Using a string means its value will be replaced and not composed
default[cookbook_name]['replace']['whitelist'] = '/'

# Default is everything in this cookbook path (mostly used for "file" recipe)
default[cookbook_name]['eval']['whitelist'] = [
  "/#{cookbook_name}"
]

# Configuration for file recipe which take an attributes path and creates a
# file containing them in the defined format

# Default format for generate files, it can also be specified by 'format'
# attribute in 'file' or by the filename extension
default[cookbook_name]['default_format'] = 'txt'
default[cookbook_name]['file'] = {
  # # filename with absolute path
  # '/home/user/filename.ext' => {

  #   # content can be a hash, a string or a replace/eval expression
  #   'content' => 'node['dynamic']['filename']',

  #   # format is used to transform content to a known format
  #   # currently supported: yaml, json, xml, ini and txt (aka do nothing)
  #   # if not specified, it uses the filename extension or is set to default
  #   'format' => 'xml',

  #   # create the directory containing the file, deactivated by default
  #   # use true for default value (recursive: true)
  #   'directory' => true,
  #   # or a hash to specify directory resource properties
  #   'directory' => {
  #     'path' => '/my/directory/path',
  #     'mode' => '0700',
  #     'recursive' => 'false'
  #   }

  #   # options for format, array of arguments sent to the different export
  #   'options' => [{ 'RootName' => 'configuration' }],

  #   # any options found in file resource
  #   'owner' => 'root',
  #   'group' => 'root',
  #   'mode' => '0600'
  #
  # }
}
