#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require_relative '../libraries/eval_string_parser.rb'

# rubocop:disable Lint/InterpolationCheck
intrpls = {
  '#{test}' => [{ 'intrpl' => '#{test}' }],
  '#{\'test\'}' => [{ 'intrpl' => [{ 'sq_string' => '\'test\'' }] }],
  '#{"test"}' => [{ 'intrpl' => [{ 'dq_string' => '"test"' }] }],
  '#{"\"test\""}' => [{ 'intrpl' => [{ 'dq_string' => '"\"test\""' }] }],
  '#{\\\"\"test\""}' => [{ 'intrpl' => [{ 'dq_string' => '"\"test\""' }] }],
  '#{"\\\\\"\""}' => [{ 'intrpl' => [{ 'dq_string' => '"\\\\\"\\""' }] }],
  '#{h = {}}' => [{ 'intrpl' => [{ 'hash' => '{}' }] }],
  '#{h = "}"}' => [{ 'intrpl' => [{ 'dq_string' => '"}"' }] }],
  '#{h = { {"test" => "{}"} }}' =>
  [{ 'intrpl' =>
     [{ 'hash' =>
        [{ 'hash' => [{ 'dq_string' => '"test"' },
                      { 'dq_string' => '"{}"' }] }] }] }]
}

not_intrpls = {
  '' => '',
  'test' => [{ 'chars' => 'test' }],
  '{test}' => [{ 'chars' => '{test}' }],
  '#{test' => [{ 'chars' => '#{test' }],
  '#{test}a' => [{ 'intrpl' => '#{test}' }, { 'chars' => 'a' }],
  '#{test"}"' => [{ 'chars' => '#{test' },
                  { 'dquote' => '"' },
                  { 'chars' => '}' },
                  { 'dquote' => '"' }]
}

mixed = {
  'dq:", sq:\' interpol:#{"pre-#{4+7}-\"in\""}' =>
  [
    { 'chars' => 'dq:' },
    { 'dquote' => '"' },
    { 'chars' => ', sq:\' interpol:' },
    { 'intrpl' => [{ 'dq_string' => [{ 'intrpl' => '#{4+7}' }] }] }
  ]
}
# rubocop:enable Lint/InterpolationCheck

parser = EvalStringParser.new
describe "#{parser.class} should parse" do
  intrpls.each_pair do |test, result|
    it "`#{test}' as a root interpolation" do
      res = parser.parse(test)
      expect(res).to eq(result)
    end
  end

  not_intrpls.each do |test, result|
    it "`#{test}' not as a root interpolation" do
      res = parser.parse(test)
      expect(res).to eq(result)
    end
  end

  mixed.each_pair do |test, result|
    it "`#{test}' as containing a double quote and an interpolation" do
      res = parser.parse(test)
      expect(res).to eq(result)
    end
  end
end
