Changelog
=========

2.2.0
-----

Main:

- fix: mixed double interpolation evaluation
  + 'a-#{"#{1+2}"}' was not evaluated correctly because we escaped the
    double quotes. Extend the parser to escape only the quote which need
    to be escaped.

Misc:

- style(rubocop): add empty line after guard clauses
- chore: add supermarket category in .category

2.1.0
-----

Main:

- feat: can create dir when generating a file
- fix: display errors when not using default recipe

2.0.0
-----

Main:

- feat: make eval return real object instead of string
  + If the evalued code return a hash, the attribute will be a hash and
    not the string representation of the hash. Same for an int.
  + WARNING: this may break existing code relying on previous (broken)
    behavior. Add `.to_s` to evalued code if you need.
- feat: raise an error if replaced attr is not found
  + WARNING(breaking change): raise an exception if target value does not exist
- feat: aggregate all errors including eval ones before raising them
- feat: output all substitutions with Chef::Log.info
- feat: add recipe to generate a file from attrs
  + You can choose a predefined format (txt, yaml, json, ini, xml) or use
    the dynamic eval to generate the string you want.
- chore: set chef version to 14 (for ruby 2.5)

Tests:

- test: unchefize attributes before marshaling them
- style(rubocop): disable InterpolationCheck in test
- test: include .gitlab-ci.yml from test-cookbook
- test: replace deprecated require\_chef\_omnibus

Misc:

- doc: use karma for git format in contributing
- doc: add a known limitation
- style(rubocop): fix some style issues
- chore: add 2018 to copyright notice
- chore: set generic maintainer & helpdesk email

1.1.0
-----

Main:

- Handover maintenance to Make.org: update emails, copyright, urls, etc.

Tests:

- Use Continuous Integration with gitlab-ci with latest template [20170405]

Misc:

- Fix all rubocop offenses
- Use cookbook\_name alias in attributes

1.0.1
-----

- Fix non-intuitive behavior with escaped double-quote, add a test-case
- Simplify the whitelist defined for eval tests
- Clarify documentation on eval recipe

1.0.0
-----

- Initial version, supports eval and replace with configurable whitelists
