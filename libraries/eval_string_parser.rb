#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'parslet'

# Parser class
class EvalStringParser < Parslet::Parser
  rule(:hash_content) { (string | hash | str('}').absent? >> any).repeat }
  rule(:sq_content) { (str("\\'") | str("'").absent? >> any).repeat }
  rule(:dq_content) { (intrpl | str('\"') | str('"').absent? >> any).repeat }

  rule(:hash) { (str('{') >> hash_content >> str('}')).as('hash') }
  rule(:intrpl) { (str('#{') >> hash_content >> str('}')).as('intrpl') }
  rule(:dq) { (str('"') >> dq_content >> str('"')).as('dq_string') }
  rule(:sq) { (str("'") >> sq_content >> str("'")).as('sq_string') }
  rule(:string) { sq | dq }

  rule(:main_string) do
    (intrpl | str('"').as('dquote') |
     (any >> ((str('#{') | str('"')).absent? >> any).repeat).as('chars')
    ).repeat
  end
  root(:main_string)
end
