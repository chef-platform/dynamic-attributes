#
# Copyright (c) 2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require_relative 'eval_string_parser'

# Module to be mixined with recipe to be used
module DynamicAttributes
  # Sub-module to be able to select exactly what we want
  module Eval
    include DynamicAttributes
    def eval_attributes(attributes_kind, whitelist)
      evaluator = lambda do |key, value|
        value.is_a?(String) ? eval_value(key, prepare(value)) : value
      end
      rec_forest(node.send(attributes_kind), whitelist, evaluator)
    end

    def eval_value(key, to_eval)
      # rubocop:disable Security/Eval
      eval(to_eval, binding, __FILE__, __LINE__)
      # rubocop:enable Security/Eval
    rescue StandardError => e
      raise_error "#{key} failed => cannot eval `#{to_eval}`, caused by #{e}!"
      to_eval
    end

    # Escape " to avoid any conflict with surrounding ones we add
    def prepare(value)
      parsed = EvalStringParser.new.parse(value)
      return "\"#{value}\"" unless parsed.is_a?(Array)

      if parsed.size == 1 && parsed.first.keys == ['intrpl']
        value[2..-2]
      else
        offsets = offsets_from_splice(parsed)
        add_escape(value, offsets)
      end
    end

    def offsets_from_splice(parsed)
      parsed.map do |item|
        type, splice = item.to_a.flatten
        splice.offset if type == 'dquote'
      end.compact
    end

    def add_escape(string, offsets)
      last, accstr = offsets.reduce([0, '']) do |memo, offset|
        [offset, "#{memo.last}#{string[memo.first, offset]}\\"]
      end
      "\"#{accstr}#{string[last..-1]}\""
    end
  end
end
