name 'dynamic-attributes'
maintainer 'Chef Platform'
maintainer_email(
  'incoming+chef-platform/dynamic-attributes@incoming.gitlab.com'
)
license 'Apache-2.0'
description 'Interprets and Rewrites Node Attributes'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/dynamic-attributes'
issues_url 'https://gitlab.com/chef-platform/dynamic-attributes/issues'

version '2.3.0'

chef_version '>= 14.0'

# Should support anything as it is pure ruby
supports 'redhat'
supports 'centos'
supports 'scientific'
supports 'fedora'
supports 'debian'
supports 'ubuntu'
supports 'suse'
