#
# Copyright (c) 2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Include all active recipes

# To aggregate all errors that could occur, we rescue 'replace' before raising
# an exception including all errors
errors = %w[replace eval].reduce([]) do |acc, type|
  begin
    include_recipe "#{cookbook_name}::#{type}"
    acc
  rescue DynamicAttributes::DynamicAttributesErrors => e
    acc + e.messages
  end
end

unless errors.empty?
  message = (["#{cookbook_name} errors:"] + errors).join("\n  ")
  raise message
end

# Generate files from attributes
include_recipe "#{cookbook_name}::file"
