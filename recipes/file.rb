#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# First, install needed gems
package_retries = node[cookbook_name]['package_retries']
chef_gem 'xml-simple' do
  compile_time true
  retries package_retries unless package_retries.nil?
end

%w[pp yaml json xmlsimple].each { |p| ::Chef::Recipe.send(:require, p) }

# Little hack to use internal to_ini method of systend_unit resource
def to_ini(content, filename)
  systemd_unit "#{cookbook_name}::#{filename}.to_ini" do
    content content
    action :nothing
  end.to_ini
end

# Use pp to pretty print (or keep the original string)
def to_pp(content, opts)
  content.is_a?(String) ? content : ::PP.pp(content, '', *opts)
end

# Return content as a string in given format
def transform_to(content, format, opts, filename)
  # xml and yaml want options with keys with symbol
  opts = opts.map { |e| e.is_a?(Hash) ? e.transform_keys(&:to_sym) : e }
  str = case format
        when 'yaml' then content.to_yaml(*opts)
        when 'json' then ::JSON.pretty_generate(content, *opts)
        when 'xml' then ::XmlSimple.xml_out(content, *opts)
        when 'ini' then to_ini(content, filename)
        else to_pp(content, opts) # txt or anything else
        end
  # Add a last \n if needed
  "#{str.chomp("\n")}\n"
end

default_format = node[cookbook_name]['file']['default_format']
node[cookbook_name]['file'].to_h.each_pair do |filename, config|
  unless config['directory'].nil? && !config['directory']
    dir_r = directory "#{cookbook_name}: create dir for #{filename}" do
      path File.dirname(filename)
      recursive true
    end
    if config['directory'].is_a?(Hash)
      config['directory'].each { |k, v| dir_r.send(k, v) }
    end
  end

  resource = file filename
  format = config['format'] || filename.split('.').last || default_format
  opts = config['options'] || []
  config['content'] = transform_to(config['content'], format, opts, filename)
  config
    .reject { |k, _| %w[format options directory].include?(k) }
    .each_pair { |key, value| resource.send(key, *value) }
end
