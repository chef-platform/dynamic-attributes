#
# Copyright (c) 2016 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

def unchefize(hash)
  hash.map do |k, v|
    newv = if v.is_a?(Hash)
             unchefize(v)
           else
             v.class.name.include?('Chef') ? v.class.name : v
           end
    [k, newv]
  end.to_h
end

to_dump = unchefize(node.attributes.to_h)

file '/root/attributes' do
  content Marshal.dump(to_dump)
end
