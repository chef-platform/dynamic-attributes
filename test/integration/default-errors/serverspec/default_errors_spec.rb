#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

expected_errors = {
  'replace-single-error' =>
    "node['dynamic']['replace-single-error'] failed "\
    "=> node['doesnotexist'] is nil!\n",
  'replace-nested-error' =>
    "node['dynamic']['replace-nested-error'] failed "\
    "=> node['doesnotexist']['notthere'] does not exist!\n",
  'eval-simple-error' =>
    "node['dynamic']['eval-simple-error'] failed => cannot eval "\
    "`node['doesnotexist']['nested']`, "\
    "caused by undefined method `[]' for nil:NilClass!\n",
  'eval-composed-error' =>
    "node['dynamic']['eval-composed-error'] failed => cannot eval "\
    '`"s-#{node[\'doesnotexist\'][\'nested\']}"`'\
    ", caused by undefined method `[]' for nil:NilClass!\n"
}

describe 'Error management' do
  expected_errors.each do |error, message|
    it "#{error} should be raised" do
      expect(file('/root/error').content).to include(message)
    end
  end
end
