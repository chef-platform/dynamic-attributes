#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Directory should be created correctly' do
  it '/root/secrets' do
    expect(file('/root/secrets')).to exist
    expect(file('/root/secrets')).to be_directory
  end

  it '/root/yamldir' do
    expect(file('/root/yamldir')).to exist
    expect(file('/root/yamldir')).to be_directory
    expect(file('/root/yamldir')).to be_mode 700
  end
end

# rubocop:disable Metrics/BlockLength
describe 'Check file output with the following format exports' do
  it 'TXT (aka identity) with a 40 col wrapping, with no type nor extension' do
    expect(file('/root/secrets/mysecrets').content)
      .to eq(<<-TXT.gsub(/^ {6}/, ''))
      {"login"=>"iamtotoandiamhappy",
       "pass"=>"supersecurepassword",
       "url"=>
        "http://localhost:8080/myservice/login",
       "args"=>["arg1", "arg2"],
       "opts"=>
        {"opt1"=>"val1", "opt2"=>"val2"}}
    TXT
  end

  it 'TXT with a to_yaml in the evaluated content with an unknown extension' do
    expect(file('/root/mysecrets.content').content)
      .to eq(<<-YAML.gsub(/^ {6}/, ''))
      ---
      login: iamtotoandiamhappy
      pass: supersecurepassword
      url: http://localhost:8080/myservice/login
      args:
      - arg1
      - arg2
      opts:
        opt1: val1
        opt2: val2
    YAML
  end

  it 'YAML with an indentation of 4 and a defined type set to yaml' do
    expect(file('/root/yamldir/mysecrets.yaml').content)
      .to eq(<<-YAML.gsub(/^ {6}/, ''))
      ---
      login: iamtotoandiamhappy
      pass: supersecurepassword
      url: http://localhost:8080/myservice/login
      args:
      - arg1
      - arg2
      opts:
          opt1: val1
          opt2: val2
    YAML
  end

  it 'JSON with an indentation of "    ", using extension for type' do
    expect(file('/root/mysecrets.json').content)
      .to eq(<<-JSON.gsub(/^ {6}/, ''))
      {
          "login": "iamtotoandiamhappy",
          "pass": "supersecurepassword",
          "url": "http://localhost:8080/myservice/login",
          "args": [
              "arg1",
              "arg2"
          ],
          "opts": {
              "opt1": "val1",
              "opt2": "val2"
          }
      }
    JSON
  end

  it 'XML with "configuration" as root, no attrs and using the extension' do
    expect(file('/root/mysecrets.xml').content)
      .to eq(<<-XML.gsub(/^ {6}/, ''))
      <configuration>
        <login>iamtotoandiamhappy</login>
        <pass>supersecurepassword</pass>
        <url>http://localhost:8080/myservice/login</url>
        <args>arg1</args>
        <args>arg2</args>
        <opts>
          <opt1>val1</opt1>
          <opt2>val2</opt2>
        </opts>
      </configuration>
    XML
  end

  it 'INI with no special option, forcing the type to ini' do
    expect(file('/root/mysecrets.ini').content)
      .to eq(<<-INI.gsub(/^ {6}/, ''))
      [credentials]
      login = toto
      pass = superpass

      [configuration]
      url = localhost
    INI
  end
end
# rubocop:enable Metrics/BlockLength
